package evidenceset;

import chains.Builder;
import com.carrotsearch.sizeof.RamUsageEstimator;
import evidenceset.build.EvidenceSetBuilder;
import evidenceset.build.Operator;
import input.ColumnPair;
import input.ParsedColumn;
import predicates.Predicate;
import predicates.sets.PredicateBitSet;

import java.util.*;

import static chains.Builder.Tuples;
import static predicates.PredicateBuilder.predicates;

public class Repair{
	private List<Builder> chains;
	public Map<Integer , String> column;

	public Repair(List<Builder> chains,Map<Integer , String> column) {
		this.chains=chains;
		this.column=column;
	}

	public IEvidenceSet getevidence(int[][] data,int[][] add_data,Collection<ColumnPair> pairs) throws Exception {
		berfind bf=new berfind(chains, data, add_data,pairs);
		return bf.evidence;
	}
}
